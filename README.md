This is **PyJLA**, a Python wrapper for JLA likelihoods.

Copyright 2018 Francesco Montanari

Home page: http://fmnt.info/projects/

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*Note*: the original C++ JLA code is distributed under a different
license.

# Descritpion

PyJLA is a Python3 wrapper for the [SDSS-II/SNLS3 Joint Light-curve
Analysis](http://supernovae.in2p3.fr/sdss_snls_jla/ReadMe.html) C++
likelihood code.

The original likelihood code is unchanged (except for a new Makefile
that builds a shared library). The Python3 wrapper provides the same
full and simplified likelihood classes as the C++ code.

# Dependencies

The following software is required to build the library:

- [BLAS](http://www.netlib.org/blas/).
- [LAPACK](http://www.netlib.org/lapack/).
- [SWIG](http://www.swig.org/) (>= 3).

Debian users: install the dependencies as follows.

```shell
apt install libblas3 libblas-dev liblapack3 liblapack-dev swig
```

## Optional

This distribution uses GNU Autotools. If you are getting the sources
from git (or change `configure.ac`), you'll need to have these tools
installed to (re)build. All of these programs are available from
<ftp://ftp.gnu.org/gnu>.

# Install

Standard GNU build system:

```shell
./configure
make
make install
```

This will install a C++ shared library `libjla`, and a Python3 module `pyjla`.

## Bootstrap

If you are getting the sources from git (or change `configure.ac`),
launch the script `autogen.sh` to reconfigure the Makefile's.

# Uninstall

```shell
make uninstall
```

# Usage

See [examples/getting_started.ipynb](examples/getting_started.ipynb).
