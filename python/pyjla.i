/* Copyright 2018 Francesco Montanari
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you
 * may not use this file except in compliance with the License.  You
 * may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
*/

%module pyjla

%rename(right_shift) operator >>;

%include "std_iostream.i"
%include "std_string.i"
%include "std_vector.i"

%template(vectord) std::vector<double>;

%{
#include <vector>
#include "jla.h"
%}

%feature("autodoc", "3");
%include "jla.h"

/* It is difficult to wrap `double*', especially as return
 * types. (Pointers to arrays passed as `double*' arguments may be
 * handled with `carrays.i', which however requires use of low-level
 * functions in Python.)
 *
 * Hence, create wrappers to replace `double*' by
 * `std::vector<double>' (the latter being immediately compatible with
 * Python lists).
 */

// Change of return type requires renaming.
%rename(getZ) wrap_getZ;

%inline %{
  class Likelihood: public JLALikelihood
  {
  public:
    using JLALikelihood::computeLikelihood;

    Likelihood(int verbosity=2): JLALikelihood(verbosity) {}

    std::vector<double> wrap_getZ ()
    {
      double* z = getZ();
      size_t nz = size();
      std::vector<double> vec(nz);
      for (auto i=0; i<nz; ++i)
        vec[i] = z[i];
      return vec;
    }

    double computeLikelihood (std::vector<double> distanceModulii,
                              std::vector<double> nuisanceParameters)
    {
      double* mu = distanceModulii.data();
      double* params = nuisanceParameters.data();
      return computeLikelihood (mu, params);
    }
  };

  class SimplifiedLikelihood: public SimplifiedJLALikelihood
  {
  public:
    using SimplifiedJLALikelihood::computeLikelihood;

    // Constructor not inherited automatically.
    SimplifiedLikelihood(int verbosity=2): SimplifiedJLALikelihood(verbosity) {}

    // Change of return type requires renaming it.
    std::vector<double> wrap_getZ ()
    {
      double* z = getZ();
      size_t nz = size();
      std::vector<double> vec(nz);
      for (auto i=0; i<nz; ++i)
        vec[i] = z[i];
      return vec;
    }

    double computeLikelihood (std::vector<double> distanceModulii,
                              std::vector<double> nuisanceParameters)
    {
      double* mu = distanceModulii.data();
      double* params = nuisanceParameters.data();
      return computeLikelihood (mu, params);
    }
  };

%}
